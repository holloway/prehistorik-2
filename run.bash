#!/bin/bash

print_guide() {
	echo "to install --> ./run.bash -i"
	echo "to run     --> ./run.bash -r"
}

install_dependencies() {
	sudo apt-get install dosbox -y
}

run_program() {
	dosbox ./pre2.exe
}

main() {
	case $1 in
	"-r")
		run_program
		;;
	"-i")
		install_dependencies
		;;
	*)
		print_guide
		;;
	esac
}

main $@
