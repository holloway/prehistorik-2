# Prehistorik 2 - DOS Game
![Prehistorik 2](https://lh3.googleusercontent.com/KLoQIKSSD0Md9Fii6_qZsZDRDXnk_Phv-YtQMHunDaRGBlxwdYx0cNCf0t2ZtaVEhYFa9r5WyeZNnDFXIbHFMZDkIFvncyGuAmEX7u-MHWGqeoLSqWEVKhovo1F5ZqjYVuZO59bjw0ovZF3TRLy_a61RTId5KHlSQMSoZ1S8jJowQBb0pbuuLefoHf72NzcPqaLiNK2uUL4YVeeoaM6BzUU-UMXWRBCbeHFiM9vZHlsUgwmCVucUHBF7jUh3p4CqQLD-CeiMxk5zIjs4Izo4RRs4HqJsK4NBVuauEnQKC94ZoJjt1DIuLMBEt9dVtGpkGY8EXTYJJHJDkvLDxvlcYIg4nvqI6yfyN5H1OeWRRxIldfxAr50S6UjjDGLNxsEDFpZ3lf1LjVyOv5sjPdIp2Vz6hRYKbpBPvIl6qDp8-UVWO-sayoQMMaMyvlBjfS6Wq9zZeQH0JaeqdsZgXc_HuBuuUgaJ2Z8my08rcKjgNqw7wyZTnBnVLbrOI0wscAQITbi6ZCyt4MlQ-2Cs1Izy9I7kuQjjYnJn7J57CY_nU1h94n1qrc8QkOj7dt3Bv-q9O9Yo3Gnnbb7JnHRUleB9p2AhhknQ5gsAhZ-cuF-wismXzoJ78SXjfHOlxLtkebk_7D0yDmAy_g-CPmU1qDjPZujo9KiBZRQ7yaHY=w270-h360-no)

The classic 1992 game that works till today! This is a full pack comes with runing and installation script.

# To install
This game requires DosBox. It has been scripted for you so do:
```
$ sudo ./run.bash -i
```

# To run
To run, use:
```
$ ./run.bash -r
```

# License
Proprietary to Titus Interactive (1993)
